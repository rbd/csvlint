# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
BEGIN {
    sep  = ","
    quo  = "'"
    FPAT = "([^"sep"]*)|("quo"[^"quo"]+"quo")";
    nf   = -1;
}
{
    if (nf == -1) nf = NF;
    else if (NF != nf) {
	printf "%s:%d\tBad number of fields (%d instead of %d)\n",\
	    FILENAME, NR, NF, nf;
    }
}
/^,$/ {
    printf "%s:%d\tNo data\n", FILENAME, NR;
}
