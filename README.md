# csvlint
Given an ill-formed csv file:
```
Code,Name
ALB
ATG,'Antigua, Barbuda',
ABW,Aruba,Arube

,
```
you got
```
$ gawk -f csvlink.awk bad.csv
bad.csv:2 Bad number of fields (1 instead of 2)
bad.csv:3 Bad number of fields (3 instead of 2)
bad.csv:4 Bad number of fields (3 instead of 2)
bad.csv:5 Bad number of fields (0 instead of 2)
bad.csv:6 No data
```
